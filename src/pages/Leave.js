import React, { Component } from 'react';
import './Leave.css';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import { Button } from 'antd/lib/radio';
import { Row } from 'antd';
import searc from '../img/magnifying-glass.png'

export default class Leave extends Component {
  constructor(props) {
    super(props)

    this.state = {

    }
    this.product = [
      { id: "11" ,emp:"Saman Aroy",Ltype:"Annual",type:"Full Day",from:"14/05/2018",to:"18/05/2018",no:"4",stat:"Open",app:"Super Admin",cre:"10/05/2018 08:45:30"},
      { id: "11" ,emp:"Saman Aroy",Ltype:"Sick",type:"Full Day",from:"14/05/2018",to:"18/05/2018",no:"4",stat:"Open",app:"Super Admin",cre:"10/05/2018 08:45:30"},
      { id: "11" ,emp:"Saman Aroy",Ltype:"Personal",type:"Half Day",from:"-",to:"Afternoon",no:"4hrs.",stat:"Approves",app:"Super Admin",cre:"10/05/2018 08:45:30"},
      { id: "11" ,emp:"Saman Aroy",Ltype:"Annual",type:"Full Day",from:"14/05/2018",to:"18/05/2018",no:"4",stat:"Approves",app:"Super Admin",cre:"10/05/2018 08:45:30"},
      { id: "11" ,emp:"Saman Aroy",Ltype:"Annual",type:"Full Day",from:"14/05/2018",to:"18/05/2018",no:"4",stat:"Reject",app:"Super Admin",cre:"10/05/2018 08:45:30"},
      { id: "11" ,emp:"Saman Aroy",Ltype:"Personal",type:"Half Day",from:"Morning",to:"-",no:"4hrs.",stat:"Approves",app:"Super Admin",cre:"10/05/2018 08:45:30"},
      { id: "11" ,emp:"Saman Aroy",Ltype:"Personal",type:"Half Day",from:"-",to:"Afternoon",no:"4hrs.",stat:"Open",app:"Super Admin",cre:"10/05/2018 08:45:30"},
      
    ]
  }

  ButtonApprove=(cell,Row)=>{
    return(
      <div>
      <img src={searc} style={{marginRight:10}} />
      <button style={{border:'1px',borderRadius:'5px',height:'30px'}}>Approve</button>
      </div>
    )
  }
  render() {
    return (
      <div>
        <h3>
          <div className='lea'>
            <p>Leave Request</p>
          </div>
        </h3>
        <div>
          {/* <BootstrapTable
            data={this.product} hover
            pagination>
            <TableHeaderColumn dataField='interval_value' isKey>Product ID</TableHeaderColumn>
            <TableHeaderColumn dataField='name'>Product Name</TableHeaderColumn>
            <TableHeaderColumn dataField='price'>Product Price</TableHeaderColumn>
          </BootstrapTable> */}
          <BootstrapTable className='boot' data={this.product} hover pagination>
          <TableHeaderColumn dataField='id' dataSort isKey>{'ID'}</TableHeaderColumn>
          <TableHeaderColumn dataField='emp' dataSort>{'Employee Name'}</TableHeaderColumn>
          <TableHeaderColumn dataField='Ltype' dataSort>{'Leave Type'}</TableHeaderColumn>
          <TableHeaderColumn dataField='type' dataSort>{'Type'}</TableHeaderColumn>
          <TableHeaderColumn dataField='from' dataSort>{'From'}</TableHeaderColumn>
          <TableHeaderColumn dataField='to' dataSort>{'To'}</TableHeaderColumn>
          <TableHeaderColumn dataField='no' dataSort>{'No. of days'}</TableHeaderColumn>
          <TableHeaderColumn dataField='stat' dataSort>{'Status'}</TableHeaderColumn>
          <TableHeaderColumn dataField='app' dataSort>{'Approver'}</TableHeaderColumn>
          <TableHeaderColumn dataField='cre' dataSort>{'Created'}</TableHeaderColumn>
          <TableHeaderColumn dataField='act' dataSort dataFormat={this.ButtonApprove} >{'Action'}</TableHeaderColumn>
        </BootstrapTable>
        </div>
      </div>
    );
  }
}