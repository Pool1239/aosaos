import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { Nav, NavItem, NavDropdown, Navbar, MenuItem,Button } from 'react-bootstrap'
import './main.css'
import Digio from '../img/digio_logo.png';
import G7 from '../img/Group 7.png'
import PS from '../img/PSthLogowithOfficeOne.png'
import PS1 from '../img/bloggif_5b0278f53f536.png'

export default class main extends Component {

  render() {
    return (
      <div className='p5-7'>
        <img src={PS} className="l4" /> <br />
        <Link to='/login'><Button className="p5-8">Log in</Button></Link>
        <footer className="l3">ProjectSoft (Thailand).Co.,Ltd. </footer>
      </div>
    )
  }
}
