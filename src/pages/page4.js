import React, { Component } from 'react';
import './Leave.css';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import { Button } from 'react-bootstrap'
import { Row } from 'antd';
import { Link } from 'react-router-dom';
import searc from '../img/magnifying-glass.png'

export default class page4 extends Component {
  constructor(props) {
    super(props)

    this.state = {

    }
    this.product = [
      { id: "11", emp: "Saman Aroy", Ltype: "saman@gmail.co.th", type: "15/05/2018", from: "4 Month", to: "projectsoft@gmail.co.th", no: "Employee" },
      { id: "11", emp: "Saman Aroy", Ltype: "saman@gmail.co.th", type: "15/05/2018", from: "4 Month", to: "projectsoft@gmail.co.th", no: "Employee" },
      { id: "11", emp: "Saman Aroy", Ltype: "saman@gmail.co.th", type: "15/05/2018", from: "4 Month", to: "projectsoft@gmail.co.th", no: "Employee" },
      { id: "11", emp: "Saman Aroy", Ltype: "saman@gmail.co.th", type: "15/05/2018", from: "Pass", to: "projectsoft@gmail.co.th", no: "Employee" },
      { id: "11", emp: "Saman Aroy", Ltype: "saman@gmail.co.th", type: "15/05/2018", from: "Pass", to: "projectsoft@gmail.co.th", no: "Employee" },
      { id: "11", emp: "Saman Aroy", Ltype: "saman@gmail.co.th", type: "15/05/2018", from: "Pass", to: "projectsoft@gmail.co.th", no: "Employee" },
    ]
  }

  ButtonApprove = (cell, Row) => {
    return (
      <div>
        <Link to='#' className="l81" >Delete</Link>
      </div>
    )
  }

  render() {
    return (
      <div>
        <div className='lea'>
          <div><h3>User (31 users)</h3></div>
          <div><Link to='/create'>+ Create User Accout</Link></div>
        </div>

        <div className="sear">
          <input type="text"  className="sear1" />
          <button type="button" className='sear2'>Search</button>
          <button type="button" className='sear2'>10 entries</button>
        </div >

        <div>
          {/* <BootstrapTable
            data={this.product} hover
            pagination>
            <TableHeaderColumn dataField='interval_value' isKey>Product ID</TableHeaderColumn>
            <TableHeaderColumn dataField='name'>Product Name</TableHeaderColumn>
            <TableHeaderColumn dataField='price'>Product Price</TableHeaderColumn>
          </BootstrapTable> */}
          <BootstrapTable className='boot' data={this.product} hover pagination>
            <TableHeaderColumn dataField='id' dataSort isKey>{'ID'}</TableHeaderColumn>
            <TableHeaderColumn dataField='emp' dataSort>{'Employee Name'}</TableHeaderColumn>
            <TableHeaderColumn dataField='Ltype' dataSort>{'Email'}</TableHeaderColumn>
            <TableHeaderColumn dataField='type' dataSort>{'Starting Date'}</TableHeaderColumn>
            <TableHeaderColumn dataField='from' dataSort>{'Probation period'}</TableHeaderColumn>
            <TableHeaderColumn dataField='to' dataSort>{'Approval Email'}</TableHeaderColumn>
            <TableHeaderColumn dataField='no' dataSort>{'Role'}</TableHeaderColumn>
            <TableHeaderColumn dataField='act' dataSort dataFormat={this.ButtonApprove} >{''}</TableHeaderColumn>
          </BootstrapTable>
        </div>
      </div>
    );
  }
}