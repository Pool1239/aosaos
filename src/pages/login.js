import React, { Component } from 'react'
import './login.css'
import { Nav, NavItem, NavDropdown, Navbar, MenuItem, Button } from 'react-bootstrap'
import app_logo from '../img/app_logo.png';
import Digio from '../img/digio_logo.png';
import { Link } from 'react-router-dom';
import PS from '../img/PSthLogowithOfficeOne.png'

export default class login extends Component {
  render() {
    return (
      <div style={{ height: '-webkit-fill-available', backgroundColor: '#f3f3f3' }}>
        <div>
          <div className='l2' >
            <img src={PS} className="l4" />
            <input type="text" name="name" placeholder="Username" className="l6" />
            <input type="text" name="name" placeholder="Password" className="l6" />
            <Link to='/leave'><Button className='l7'>Login</Button></Link>
            <Link to='/confirm' className="l8" >Forgot your Password?</Link>
            <footer className="l3">ProjectSoft (Thailand).Co.,Ltd. </footer>
          </div >
        </div>
      </div>
    )
  }
}
