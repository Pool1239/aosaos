import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { Nav, NavItem, NavDropdown, Navbar, MenuItem, Button } from 'react-bootstrap'
import './ps.css'
import G7 from '../img/Group 7.png'

export default class leave_request extends Component {
  render() {
    return (
      <div>

        <div className='navbarright1' >
          <div >
            <img src={G7} /> </div>
          <div className="dropbtn1" >
            <p style={{ fontWeight: 'bold', color: '#05cdff', margin: 'unset' }}>Adminitrator</p>
            <h5 style={{ fontWeight: 'bold', color: 'black', margin: 'unset' }}>Super Admin</h5>
          </div>
        </div>

        <div className='justifycenter'>
          <div >
            <div className='solid' />
            {/*----------------  */}
            <div style={{ margin: '20px 0' }}>
              <div className='block'>
                <div className='contentleft'>
                  <p style={{ fontWeight: 'bold', marginBottom: '0px' }}> Annual Leave </p>
                  <p> ลาพักร้อน </p>
                </div>
                <div className='contentright'>
                  <span> 5/10 </span>
                  <label>Days</label>
                </div>
              </div>
            </div>
            {/*----------------  */}
            <div style={{ margin: '20px 0' }} >
              <div className='block'>
                <div className='contentleft'>
                  <p style={{ fontWeight: 'bold', marginBottom: '0px' }}> Personal Leave </p>
                  <p> ลากิจ </p>
                </div>
                <div className='contentright'>
                  <span> 2/5 </span>
                  <label>Days</label>
                </div>
              </div>
            </div>
            {/*----------------  */}
            <div style={{ margin: '20px 0' }}>
              <div className='block'>
                <div className='contentleft'>
                  <p style={{ fontWeight: 'bold', marginBottom: '0px' }}> Sick Leave </p>
                  <p> ลาป่วย </p>
                </div>
                <div className='contentright'>
                  <span> 7/30 </span>
                  <label>Days</label>
                </div>
              </div>
            </div>
            {/*----------------  */}
            <div style={{ margin: '20px 0' }}>
              <div className='block'>
                <div style={{padding:'0 10px'}}>
                  <p style={{ fontWeight: 'bold', marginBottom: '0px' }}> Flexible Benefit Request </p>
                  <p> คำขอเกี่ยวกับสวัสดิการอื่นๆ </p>
                </div>
              </div>
            </div>

          </div>
        </div>
      </div>
    )
  }
}
