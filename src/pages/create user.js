import React, { Component } from 'react'
import '../pages/create.css'
import {post} from '../service/api'

export default class user extends Component {

    constructor(props) {
      super(props)
    
      this.state = {
         userName: '',
         emp_name: '',
         emp_last: '',
         email:'',
         role:'1',
         pro_period: '1'
      }
    }

    _onChange = e => {
        this.setState({ [e.target.name]: e.target.value })
    
        // console.log(this.state.userName)
    }
    
    render() {
        return (
            <div>
                <div className='cu1'>
                    <h3 > Create User Account</h3>
                </div>
                <div className="App-User">

                    <input type="text" name="userName" placeholder="UserName" className="App-ub" onChange={this._onChange} />
                    <input type="text" name="emp_name" placeholder="Name" className="App-ub"  onChange={this._onChange}/>
                    <input type="text" name="emp_last" placeholder="Lastname" className="App-ub"  onChange={this._onChange}/>
                    <input type="text" name="email" placeholder="Email" className="App-ub"  onChange={this._onChange}/>

                   

                    <select id="country" name="role" placeholder="Probation Period" className="App-ub" onChange={this._onChange}>
                        <option value="1">Pass</option>
                        <option value="2">4 Mouth</option>
                    </select>

                    
                    <select id="country" name="pro_period" placeholder="User Role" className="App-ub" onChange={this._onChange}>
                        <option value="1">Admind</option>
                        <option value="2">Employee</option>
                    </select>

                    <div flexDirection="row" className='cu2'>
                        <input type="button" value="Cancle" className="App-button1"  />
                        <input type="button" value="Create Account" className="App-button" onClick={this._onClick} />
                    </div>
                
                </div>
            </div>

        )
    }

    _onClick = async () => {
        const { userName, emp_name, emp_last, email, pro_period, role } = this.state

        const obj = {
            userName, emp_name, emp_last, email, pro_period, role
        }
        console.log(obj)
        

        try {
            await post('/insert_user', obj)
            alert('create success')
        } catch (error) {
            alert(error)
        }
    }
}
