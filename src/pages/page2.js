import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { Nav, NavItem, NavDropdown, Navbar, MenuItem, Button } from 'react-bootstrap'
import './page2.css'
import G7 from '../img/Group 7.png'


export default class page2 extends Component {
  render() {
    return (
      <div style={{ background: '#F5F5F5' }}>
     
        <div className='justify-center' >
          <div className='widthstand'>

            <div className=' col-md-12'>
              <div className=' col-md-4'> </div>
              <div className=' col-md-1'><img src={G7} /> </div>
              <div className=' col-md-3'><p style={{ color: '#05cdff' }}>Empoyee</p><h4>Jeeranan Hahaha</h4>  </div>
              <div className=' col-md-4'> </div>
            </div>

            
            <div className='testcolumn col-md-12'>
              <div><h3 className='h33'>Public Holiday</h3></div>

              <div className='col-md-6'>
              <a href="login"className="tb"><Link to='/login'></Link >
               <div className='col1' >
                  <div className='colleft'> <p> <h3>Annual Leave</h3> ลาหยุดประจำปี </p> </div>
                <div className='colright'><text className='day'>5/10 </text> Days</div> 
              </div></a>
            </div>
            <div className='col-md-6'>
            <a href="login"className="tb"><Link to='/login'></Link >
            <div className='col1'>
                <div className='colleft'><p><h3>Enter for monkhod</h3>ลาบวช</p></div>
                <div className='colright'><text className='day'>0/15 </text>Days</div>
              </div></a>
            </div>

            <div className='col-md-6'>
            <a href="login"className="tb"><Link to='/login'></Link >
            <div className='col1'>
                <div className='colleft'><p><h3>Personal Leave</h3>ลากิจ </p></div>
                <div className='colright'><text className='day'>2/5 </text>Days</div>
              </div></a>
            </div>
            <div className='col-md-6'>
            <a href="login"className="tb"><Link to='/login'></Link >
            <div className='col1'>
                <div className='colleft'><p><h3>Military Leave </h3> ลาเพื่อรับราชการ</p></div>
                <div className='colright'><text className='day'>0/60 </text>Days</div>
              </div></a>
            </div>

            <div className='col-md-6'>
            <a href="login"className="tb"><Link to='/login'></Link >
              <div className='col1'>
                <div className='colleft'><p><h3>Sick Leave</h3>ลาป่วย</p></div>
                <div className='colright'><text className='day'>7/30 </text>Days</div>
              </div></a>
            </div>
            <div className='col-md-6'>
            <a href="login"className="tb"><Link to='/login'></Link >
              <div className='col1'>
                <div className='colleft'><p><h3>Maternity Leave</h3>ลอคลอด</p></div>
                <div className='colright'><text className='day'>0/90 </text>Days</div>
              </div></a>
            </div>
            <div className='col-md-6'>
            <a href="login"className="tb"><Link to='/login'></Link >
              <div className='col1'>
                <div className='colleft1'><p><h3>Flexible Benefit Request</h3>คำขอเกี่ยวกับสวัสดิการอื่นๆ</p></div>
                {/* <div className='colright'><p> </p></div> */}
              </div></a>
            </div>
            <div className='col-md-6'  >
            <a href="login"className="tb"><Link to='/login'></Link >
              <div className='col1'>
                <div className='colleft'><p><h3>Trainig Leave</h3>ลาเพื่อฝึกอบรม</p></div>
                <div className='colright'><p><text className='day'>0/5 </text>Days</p></div>
              </div></a>
            </div>

          </div>
        </div>
      </div> 

     
        </div >
        );
  }
}
