import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { Nav, NavItem, NavDropdown, Navbar, MenuItem,Button } from 'react-bootstrap'
import './navbar.css'
import Digio from '../img/digio_logo.png';
import G7 from '../img/Group 7.png'
import Project from '../img/Project.png'
import PS from '../img/PSthLogowithOfficeOne.png'
export default class navbar extends Component {
  render() {
    return (
      <div>
        <div className="nav3">
          <img src={Project} className='nav4'/>
          <Link to='/leave'><Button bsStyle="info" className='nav2'>Leave Request</Button></Link>
          <Button bsStyle="info" className='nav2'>User</Button>
          <Button bsStyle="info" className='nav2'>Report</Button>
          <Button bsStyle="info" className='nav2'>My Leave Request</Button>
          <Link to='/login'><Button bsStyle="info" className='nav2'>Log out</Button></Link>
          <Button bsStyle="info" className='nav6'>Admin<br/>Jakkapan Junsawang  </Button>
          <img src={G7} className='nav5' />  
        </div>        
        <footer className="nav1">ProjectSoft (Thailand).Co.,Ltd. </footer>      
      </div>
    )
  }
}
