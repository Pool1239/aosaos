import React, { Component } from 'react'
import { Link, withRouter } from 'react-router-dom'
import { Nav, NavItem, NavDropdown, Navbar, MenuItem, Button } from 'react-bootstrap'
import Digio from '../img/digio_logo.png';
import G7 from '../img/Group 7.png'
import Project from '../img/Project.png'
import PS from '../img/123456.png'

class nobar extends Component {
  render() {
    const { pathname } = this.props.location
    return (
      <div>
        <div className='ul'>
          {/* =========================== */}
          <div className='navbarleft' >
            <div className='topleft'>
              <img src={PS} />
            </div>
            <div className='topright'>
              <Link to='/leave' style={pathname === '/leave' ? { background: 'rgba(0, 94, 126, 0.76)', height: '100%' } : null} className="dropbtn">Leave Request</Link>
              <Link to='/page4' style={pathname === '/page4' ? { background: 'rgba(0, 94, 126, 0.76)' } : null} className="dropbtn">User</Link>
              <li className="dropdown">
              <Link to='/leavereport' style={pathname === 'leavereport' ? { background: 'rgba(0, 94, 126, 0.76)' } : null} className="dropbtn">Report</Link>
                <div className="dropdown-content">
                  <Link to='/leavereport' style={pathname === '/leavereport' ? { background: 'rgba(0, 94, 126, 0.76)' } : null} className="dropbtn">Leave Report</Link>
                  <Link to="/benefitreport" style={pathname === '/benefitreport' ? { background: 'rgba(0, 94, 126, 0.76)' } : null} className="dropbtn">Flexible Benefit Report</Link>
                </div>
              </li>
              <Link to='/leaverequest' style={pathname === '/leaverequest' ? { background: 'rgba(0, 94, 126, 0.76)' } : null} className="dropbtn">My Leave Request</Link>
            </div>
          </div>
          {/* ==================== */}
          <div className='navbarright'>
            <div>
              <img src={G7} />
            </div>
            <div className="dropbtn1" >
              <p> Admin </p>
              <p> Super Admin </p>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default withRouter(nobar)