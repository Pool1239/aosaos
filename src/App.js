import React, { Component } from 'react';
import './App.css';
import Nobar from './component/nobar'
class App extends Component {
  render() {
    const { children } = this.props
    return (
      
      <div>
        <Nobar />
        {
          children
        }
        <footer className="App7">ProjectSoft (Thailand).Co.,Ltd. </footer>
      </div>
    );
  }
}
export default App;