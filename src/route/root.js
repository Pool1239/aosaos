import React, { Component } from 'react'
import { Route, BrowserRouter, Switch } from 'react-router-dom'
import login from '../pages/login'
import AppRoute from './app'
import confirm from '../pages/confirm'

export default () => (
    <BrowserRouter>
    <Switch>
    <Route exact path='/' component={login} />
    <Route path='/confirm' component={confirm} />
    <AppRoute></AppRoute>
    </Switch>
    </BrowserRouter>
)
