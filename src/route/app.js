import React, { Component } from 'react'
import { Route } from 'react-router-dom'

import App from '../App'
import navbar from '../component/navbar'
import page4 from '../pages/page4'
import leave from '../pages/Leave'
import report from '../pages/report'
import leavereport from '../pages/leave report'
import benefitreport from '../pages/benefit report'
import leaverequest from '../pages/leave request'
import create from '../pages/create user'
// import page2 from '../pages/page2'


export default () => <App>
    <Route path='/navbar' component={navbar} />
    <Route path='/page4' component={page4}/>
    <Route path='/leave' component={leave}/>
    <Route path='/report' component={report}/>
    <Route path='/leavereport' component={leavereport}/>
    <Route path='/benefitreport' component={benefitreport}/>
    <Route path='/leaverequest' component={leaverequest}/>
    <Route path='/create' component={create}/>
    
</App>
